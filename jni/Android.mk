LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE     := native-activity
LOCAL_SRC_FILES  := main.c
LOCAL_C_INCLUDES :=
LOCAL_CFLAGS     := -std=gnu99 -D_GNU_SOURCE -falign-functions=4 -ffast-math -fno-strict-aliasing -DNDEBUG
LOCAL_LDLIBS     := -llog -landroid
LOCAL_ARM_MODE   := arm
LOCAL_STATIC_LIBRARIES := android_native_app_glue

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/native_app_glue)
